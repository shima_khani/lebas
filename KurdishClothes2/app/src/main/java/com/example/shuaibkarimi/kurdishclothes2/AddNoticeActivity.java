package com.example.shuaibkarimi.kurdishclothes2;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.renderscript.ScriptIntrinsicYuvToRGB;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.shuaibkarimi.kurdishclothes2.app.AppController;
import com.example.shuaibkarimi.kurdishclothes2.model.Notice;
import com.example.shuaibkarimi.kurdishclothes2.services.AddNoticeService;
import com.example.shuaibkarimi.kurdishclothes2.services.Services;
import com.example.shuaibkarimi.kurdishclothes2.utils.SessionManager;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.UUID;

public class AddNoticeActivity extends AppCompatActivity {

    LinearLayout linearLayout;
    ImageView imageView;
    EditText title, price, color, note;
    Button btnChooseImage, btnOK, btnUpload, btnOkFinal;
    long noticeId;

    private Uri filePath;
    private Bitmap bitmap;
    String a;

    /*AddNoticeService addNoticeService*/;

    SessionManager sessionManager;


    private static final int IMAGE_REQUEST_CODE = 3;
    private static final int STORAGE_PERMISSION_CODE = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_notice);


        /*addNoticeService = new AddNoticeService(this);*/

        sessionManager = new SessionManager(this);

        requestStoragePermission();

        linearLayout = (LinearLayout) findViewById(R.id.linear_add_image);
        title = (EditText) findViewById(R.id.add_title);
        price = (EditText) findViewById(R.id.add_price);
        color = (EditText) findViewById(R.id.add_color);
        note = (EditText) findViewById(R.id.add_note);

        btnOkFinal = (Button) findViewById(R.id.btn_ok_final);
        btnOkFinal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
            }
        });


        btnChooseImage = (Button) findViewById(R.id.btn_add_image);
        btnChooseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Complete action using"), IMAGE_REQUEST_CODE);

            }
        });

        btnUpload = (Button) findViewById(R.id.btn_upload_image);
        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                uploadMultipart(a);
            }
        });



        btnOK = (Button) findViewById(R.id.btn_ok_info);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                /*long userId = Long.parseLong(sessionManager.getStoredId());*/
                String id = sessionManager.getStoredId();

                long userId = Long.parseLong(id);


                Notice notice = new Notice();
                notice.setUserId(userId);
                notice.setTitle(title.getText().toString());
                notice.setPrice(Long.parseLong(price.getText().toString()));
                notice.setColor(color.getText().toString());
                notice.setNote(note.getText().toString());

                /*try {
                    addNoticeService.addNotice(notice);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }*/
                try {
                    addNotice(notice);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IMAGE_REQUEST_CODE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                /*txtPath.setText("Path: ". concat(getPath(filePath)));*/
                a = "".concat(getPath(filePath));
                /*Toast.makeText(this, "" + a, Toast.LENGTH_SHORT).show();*/

                imageView = new ImageView(getApplicationContext());
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(200, 200);
                imageView.setLayoutParams(params);
                imageView.setImageBitmap(bitmap);
                linearLayout.addView(imageView);
                /*myImage.setImageBitmap(bitmap);*/
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }


    public void uploadMultipart(String path) {

        try {
            String uploadId = UUID.randomUUID().toString();
            new MultipartUploadRequest(this, uploadId, Services.ADDNOTICEIMAGE)
                    .addFileToUpload(path, "file")
                    .addParameter("notice_id", String.valueOf(noticeId))
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(2)
                    .startUpload();
            Toast.makeText(this, "عکس با موفقیت آپلود شد", Toast.LENGTH_SHORT).show();
        } catch (Exception exc) {
            Toast.makeText(this, exc.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    public void addNotice(Notice notice) throws JSONException, UnsupportedEncodingException {

        String urlAddNotice = Services.ADDNOTICE;
        int reqType = Request.Method.POST;

        JSONObject jsonObject = new JSONObject();


        try {
            jsonObject.put("userId", notice.getUserId());
            jsonObject.put("title", notice.getTitle());
            jsonObject.put("price", notice.getPrice());
            jsonObject.put("color", notice.getColor());
            jsonObject.put("note", notice.getNote());
        }
        catch (JSONException e) {
            e.printStackTrace();
        }


        JsonObjectRequest jsonObjReqAddNotice = new JsonObjectRequest(reqType, urlAddNotice, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        if (response!=null) {

                            try {
                                noticeId = response.getLong("id");
                                parseJson(response);
                            }
                            catch (JSONException e) {

                                e.printStackTrace();
                            }
                        }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if ((error instanceof NetworkError) || (error instanceof NoConnectionError) ) {

                    Toast.makeText(getApplicationContext(),"Network Error", Toast.LENGTH_SHORT).show();
                    // progressDialog.dismiss();
                    return;
                }

                if (error instanceof TimeoutError){

                    Toast.makeText(getApplicationContext(), "TimeOut", Toast.LENGTH_SHORT).show();

                    return;
                }

                if ((error instanceof ServerError) || (error instanceof AuthFailureError)){

                    Toast.makeText(getApplicationContext(), "ServerError", Toast.LENGTH_SHORT).show();

                    return;
                }

            }
        })

        {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };


        jsonObjReqAddNotice.setRetryPolicy(new DefaultRetryPolicy(
                12000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        String tag_json_obj_add_notice = "json_obj_req_add_notice";

        AppController.getInstance().addToRequestQueue(jsonObjReqAddNotice, tag_json_obj_add_notice);


    }

    public void parseJson(JSONObject response) throws JSONException {

        boolean res = response.getBoolean("result");

        if (res) {

            Toast.makeText(this, "آگهی شما با موفقیت ثبت گردید", Toast.LENGTH_SHORT).show();

            /*Intent intent = new Intent(context.getApplicationContext(), MainActivity.class);
            // Closing all the Activities
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            context.startActivity(intent);*/


        }
        else {
            Toast.makeText(this, "متاسفانه آگهی شما ثبت نگردید", Toast.LENGTH_LONG).show();
        }

    }



    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }

    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }






}
