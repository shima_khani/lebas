package com.example.shuaibkarimi.kurdishclothes2.services;

/**
 * Created by Shuaib Karimi on 10/23/2017.
 */

public class Services {

    public static String REGISTER = "http://192.168.43.155:8080/sh1413/registerUser";
    public static String GETNOTICES = "http://192.168.43.155:8080/sh1413/notice/getAllNotice";
    public static String GETNOTICESIMAGES = "http://192.168.43.155:8080/sh1413/findAllNoticeImage/";
    public static String IMAGEURL = "http://192.168.43.155:8080/returnImage/";
    public static String IMAGEPREVIEWURL = "http://192.168.43.155:8080/returnPreviewImage/";
    public static String ADDNOTICE = "http://192.168.43.155:8080/sh1413/addNewNotice";
    public static String ADDNOTICEIMAGE = "http://192.168.43.155:8080/sh1413/addNoticeImage";
}
