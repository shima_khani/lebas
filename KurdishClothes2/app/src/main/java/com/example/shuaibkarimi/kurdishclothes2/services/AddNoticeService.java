package com.example.shuaibkarimi.kurdishclothes2.services;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.shuaibkarimi.kurdishclothes2.MainActivity;
import com.example.shuaibkarimi.kurdishclothes2.app.AppController;
import com.example.shuaibkarimi.kurdishclothes2.model.Notice;
import com.example.shuaibkarimi.kurdishclothes2.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Created by Shuaib Karimi on 10/25/2017.
 */

public class AddNoticeService {

    Context context;

    long noticeId;


    public AddNoticeService(Context context) {
        this.context = context;
    }


    public void addNotice(Notice notice) throws JSONException, UnsupportedEncodingException {

        String urlAddNotice = Services.ADDNOTICE;
        int reqType = Request.Method.POST;

        JSONObject jsonObject = new JSONObject();


        try {
            jsonObject.put("userId", notice.getUserId());
            jsonObject.put("title", notice.getTitle());
            jsonObject.put("price", notice.getPrice());
            jsonObject.put("color", notice.getColor());
            jsonObject.put("note", notice.getNote());
        }
        catch (JSONException e) {
            e.printStackTrace();
        }


        JsonObjectRequest jsonObjReqAddNotice = new JsonObjectRequest(reqType, urlAddNotice, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        if (response!=null) {

                            try {
                                /*noticeId = response.getLong("id");*/
                                parseJson(response);
                            }
                            catch (JSONException e) {

                                e.printStackTrace();
                            }
                        }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if ((error instanceof NetworkError) || (error instanceof NoConnectionError) ) {

                    Toast.makeText(context,"Network Error", Toast.LENGTH_SHORT).show();
                    // progressDialog.dismiss();
                    return;
                }

                if (error instanceof TimeoutError){

                    Toast.makeText(context, "TimeOut", Toast.LENGTH_SHORT).show();

                    return;
                }

                if ((error instanceof ServerError) || (error instanceof AuthFailureError)){

                    Toast.makeText(context, "ServerError", Toast.LENGTH_SHORT).show();

                    return;
                }

            }
        })

        {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };


        jsonObjReqAddNotice.setRetryPolicy(new DefaultRetryPolicy(
                12000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        String tag_json_obj_add_notice = "json_obj_req_add_notice";

        AppController.getInstance().addToRequestQueue(jsonObjReqAddNotice, tag_json_obj_add_notice);


    }



    public void parseJson(JSONObject response) throws JSONException {

        boolean res = response.getBoolean("result");

        if (res) {

            Toast.makeText(context, "آگهی شما با موفقیت ثبت گردید", Toast.LENGTH_SHORT).show();

            /*Intent intent = new Intent(context.getApplicationContext(), MainActivity.class);
            // Closing all the Activities
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            context.startActivity(intent);*/


        }
        else {
            Toast.makeText(context, "متاسفانه آگهی شما ثبت نگردید", Toast.LENGTH_LONG).show();
        }

    }




}
