package com.example.shuaibkarimi.kurdishclothes2.model;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.shuaibkarimi.kurdishclothes2.R;

/**
 * Created by Shuaib Karimi on 10/26/2017.
 */

public class NoticeViewHolder extends RecyclerView.ViewHolder {

    TextView title, price;
    ImageView imageView;

    public NoticeViewHolder(View itemView) {
        super(itemView);

        title = (TextView) itemView.findViewById(R.id.textViewTitle);
        price = (TextView) itemView.findViewById(R.id.textViewPrice);
        imageView = (ImageView) itemView.findViewById(R.id.imageViewRecycler);
    }

}
