package com.example.shuaibkarimi.kurdishclothes2;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.shuaibkarimi.kurdishclothes2.model.Notice;
import com.example.shuaibkarimi.kurdishclothes2.services.GetNoticesImagesService;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;

public class NoticeDetailsActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ViewPager viewPager;
    GetNoticesImagesService getNoticesImagesService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_details);

        /*recyclerView = (RecyclerView) findViewById(R.id.recyclerDetails);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, 0, false);
        recyclerView.setLayoutManager(layoutManager);*/

        Notice notice = (Notice) getIntent().getSerializableExtra("notice");
        long noticeId = notice.getNoticeId();
        /*String id = String.valueOf(noticeId);
        Toast.makeText(this, id, Toast.LENGTH_SHORT).show();*/

        /*getNoticesImagesService = new GetNoticesImagesService(this, recyclerView);

        try {
            getNoticesImagesService.getNoticeImages(noticeId);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }*/

        viewPager = (ViewPager) findViewById(R.id.myViewPager);
        getNoticesImagesService = new GetNoticesImagesService(this, viewPager);

        try {
            getNoticesImagesService.getNoticeImages(noticeId);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


    }
}
