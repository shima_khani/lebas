package com.example.shuaibkarimi.kurdishclothes2.services;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.RequestFuture;
import com.example.shuaibkarimi.kurdishclothes2.MainActivity;
import com.example.shuaibkarimi.kurdishclothes2.app.AppController;
import com.example.shuaibkarimi.kurdishclothes2.model.Notice;
import com.example.shuaibkarimi.kurdishclothes2.model.NoticeRecyclerAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shuaib Karimi on 10/26/2017.
 */

public class GetNoticesService {

    MainActivity context;
    RecyclerView recyclerView;

    public GetNoticesService(MainActivity context, RecyclerView recyclerView) {
        this.context = context;
        this.recyclerView = recyclerView;
    }


    public void getAllNotices() throws JSONException, UnsupportedEncodingException {


        String urlAllNotices = Services.GETNOTICES;
        /*Log.e("url houses****==", urlHouse) ;*/

        JsonArrayRequest jsonArrayRequest;

        RequestFuture<JSONObject> future = RequestFuture.newFuture();

        jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, urlAllNotices, null,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {

                        if (response!=null){
                            parseJson(response);
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if ((error instanceof NetworkError) || (error instanceof NoConnectionError) ) {

                    Toast.makeText(context,"Network Error", Toast.LENGTH_SHORT).show();
                    // progressDialog.dismiss();
                    return;
                }
                if (error instanceof TimeoutError){

                    Toast.makeText(context, "TimeOut", Toast.LENGTH_SHORT).show();
                    // context.getDialog().dismiss();
                    return;
                }

                if ((error instanceof ServerError) || (error instanceof AuthFailureError)){

                    Toast.makeText(context, "ServerError", Toast.LENGTH_SHORT).show();
                    return;
                }

            }
        }

        )

        {
            public String getBodyContentType()
            {
                return "application/json";
            }
        };


        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        String tag_json_arry = "json_array_req";

        AppController.getInstance().addToRequestQueue(jsonArrayRequest, tag_json_arry);

    }

    public void parseJson(JSONArray response){

        List<Notice> noticeList = new ArrayList<>();

        Log.e("Finito","size="+response.toString());


        for (int i = 0; i < response.length(); i++) {

            try {

               /* {
                    "houseId": 6,
                        "userId": 5,
                        "city": "تهران",
                        "homeAge": 5,
                        "lat": 35.335224,
                        "lon": 46.98995,
                        "room": 5,
                        "area": 70,
                        "price": 50000000,
                        "createDate": "Nov 9, 2016"
                },*/
                //   Contact c=new Contact();

                JSONObject c = (JSONObject) response.get(i);
                Log.e("objjj","ts="+c.toString());

                long userId = c.getLong("userId");
                long noticeId = c.getLong("noticeId");
                int price = c.getInt("price");
                String title = c.getString("title");
                String color = c.getString("color");
                String note = c.getString("note");
                /*String imagePreview = c.getString("imagePreview");*/


                Notice notice = new Notice();
                notice.setUserId(userId);
                notice.setNoticeId(noticeId);
                notice.setTitle(title);
                notice.setPrice(price);
                notice.setColor(color);
                notice.setNote(note);
                /*notice.setImagePreview(imagePreview);*/


                noticeList.add(i, notice);


            } catch (JSONException e) {
                Log.e("error",e.toString());
                e.printStackTrace();

            }

        }


        NoticeRecyclerAdapter noticeRecyclerAdapter = new NoticeRecyclerAdapter(context, noticeList, recyclerView);
        recyclerView.setAdapter(noticeRecyclerAdapter);
        noticeRecyclerAdapter.notifyDataSetChanged();

        Log.e("Finito","size=" + noticeList.size());
        Log.e("Finito","Finito");
    }

}
