package com.example.shuaibkarimi.kurdishclothes2.model;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.Target;
import com.example.shuaibkarimi.kurdishclothes2.MainActivity;
import com.example.shuaibkarimi.kurdishclothes2.NoticeDetailsActivity;
import com.example.shuaibkarimi.kurdishclothes2.R;
import com.example.shuaibkarimi.kurdishclothes2.services.Services;

import java.util.List;

/**
 * Created by Shuaib Karimi on 10/26/2017.
 */

public class NoticeRecyclerAdapter extends RecyclerView.Adapter<NoticeViewHolder> {

    RecyclerView recyclerView;
    private List<Notice> list;
    MainActivity context;


    public NoticeRecyclerAdapter(MainActivity context, List<Notice> list, RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        this.list = list;
        this.context = context;

        setListener();
    }



    @Override
    public NoticeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_recycler, parent, false);

        return new NoticeViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(NoticeViewHolder holder, int position) {

        final Notice notice = list.get(position);
        holder.title.setText(notice.getTitle());
        String pric = String.valueOf(notice.getPrice());
        holder.price.setText(pric);

        String urlImage = Services.IMAGEPREVIEWURL + notice.getNoticeId();
        Log.e("img url=",urlImage);

        Glide.with(context).load(urlImage)
                .thumbnail(0.5f)
                .crossFade()
                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.imageView);



    }


    public void setListener(){
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int i) {
                        Notice notice =  list.get(i);

                        Intent intent=new Intent(context, NoticeDetailsActivity.class)  ;
                        //   intent.putExtra("houseId",h.getHouseId());
                        intent.putExtra("notice", notice);
                        context.startActivity(intent);

                    }
                })
        );


    }


    @Override
    public int getItemCount() {
        return (null != list ? list.size() : 0);
    }
}
