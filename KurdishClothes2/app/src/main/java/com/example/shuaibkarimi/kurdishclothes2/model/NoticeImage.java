package com.example.shuaibkarimi.kurdishclothes2.model;

/**
 * Created by Shuaib Karimi on 10/23/2017.
 */

public class NoticeImage {

    private long imageId;

    private long noticeId;

    private String path;

    private int counter;





    public long getImageId() {
        return imageId;
    }

    public void setImageId(long imageId) {
        this.imageId = imageId;
    }

    public long getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(long noticeId) {
        this.noticeId = noticeId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }
}
