package com.example.shuaibkarimi.kurdishclothes2;

import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.shuaibkarimi.kurdishclothes2.utils.SessionManager;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        startTimer();
    }

    private void startTimer() {

        new CountDownTimer(4000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {

                checkSharedPerf();

            }
        }.start();
    }

    private void checkSharedPerf() {
        SessionManager sessionManager = new SessionManager(getApplicationContext());
        sessionManager.checkLogin();
    }
}
