package com.example.shuaibkarimi.kurdishclothes2.services;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.RequestFuture;
import com.example.shuaibkarimi.kurdishclothes2.NoticeDetailsActivity;
import com.example.shuaibkarimi.kurdishclothes2.app.AppController;
import com.example.shuaibkarimi.kurdishclothes2.model.NoticeImage;
import com.example.shuaibkarimi.kurdishclothes2.model.NoticeImageRecyclerAdapter;
import com.example.shuaibkarimi.kurdishclothes2.model.NoticeImageViewPagerAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shuaib Karimi on 10/26/2017.
 */

public class GetNoticesImagesService {

    NoticeDetailsActivity context;
    /*RecyclerView recyclerView;*/
    ViewPager viewPager;

    /*public GetNoticesImagesService(NoticeDetailsActivity context, RecyclerView recyclerView) {
        this.context = context;
        this.recyclerView = recyclerView;
    }*/

    public GetNoticesImagesService(NoticeDetailsActivity context, ViewPager viewPager) {
        this.context = context;
        this.viewPager = viewPager;
    }

    public void getNoticeImages(long noticeId) throws JSONException, UnsupportedEncodingException {

        String urlNoticeImages = Services.GETNOTICESIMAGES+noticeId;
        Log.e("url houses****==", urlNoticeImages) ;

        JsonArrayRequest jsonObjReq=null;

        RequestFuture<JSONObject> future = RequestFuture.newFuture();

        jsonObjReq = new JsonArrayRequest(Request.Method.GET,
                urlNoticeImages,null,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {


                        if (response!=null){

                            parseJson(response);
                        }

                    }
                    // pDialog.hide();

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if ((error instanceof NetworkError) || (error instanceof NoConnectionError) ) {

                    Toast.makeText(context,"Network Error", Toast.LENGTH_SHORT).show();
                    // progressDialog.dismiss();
                    return;
                }
                if (error instanceof TimeoutError){


                    Toast.makeText(context, "TimeOut", Toast.LENGTH_SHORT).show();
                    // context.getDialog().dismiss();
                    return;
                }

                if ((error instanceof ServerError) || (error instanceof AuthFailureError)){

                    Toast.makeText(context, "ServerError", Toast.LENGTH_SHORT).show();
                    return;
                }

            }
        }

        )

        {
            public String getBodyContentType()
            {
                return "application/json";
            }

        };


        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        String tag_json_arry = "json_array_req";

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_arry);





    }
    public void parseJson(JSONArray response){
        List<NoticeImage> noticeImagesList=new ArrayList<>();

        Log.e("Finito","size="+response.toString());


        for (int i = 0; i < response.length(); i++) {
            // JSONObject row = response.getJSONObject("result");;
            try {


               /* {
                    "houseId": 6,
                        "userId": 5,
                        "city": "تهران",
                        "homeAge": 5,
                        "lat": 35.335224,
                        "lon": 46.98995,
                        "room": 5,
                        "area": 70,
                        "price": 50000000,
                        "createDate": "Nov 9, 2016"
                },*/
                //   Contact c=new Contact();
                JSONObject c=(JSONObject) response.get(i);
                Log.e("objjj","ts="+c.toString());
                long imageId=c.getLong("imageId");

                long noticeId=c.getLong("noticeId");

                int counter=c.getInt("counter");


                NoticeImage noticeImage=new NoticeImage();
                noticeImage.setNoticeId(noticeId);
                noticeImage.setImageId(imageId);
                noticeImage.setCounter(counter);

                noticeImagesList.add(i, noticeImage);


            } catch (JSONException e) {
                Log.e("error",e.toString());
                e.printStackTrace();

            }

        }
        /*NoticeImageRecyclerAdapter adapter=new NoticeImageRecyclerAdapter(context, noticeImagesList);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();*/
        NoticeImageViewPagerAdapter adapter = new NoticeImageViewPagerAdapter(context, noticeImagesList);
        viewPager.setAdapter(adapter);


        Log.e("Finito","size="+noticeImagesList.size());
        Log.e("Finito","Finito");
    }
}
