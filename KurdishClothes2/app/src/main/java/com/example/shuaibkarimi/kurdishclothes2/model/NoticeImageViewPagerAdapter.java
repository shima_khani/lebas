package com.example.shuaibkarimi.kurdishclothes2.model;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.Target;
import com.example.shuaibkarimi.kurdishclothes2.NoticeDetailsActivity;
import com.example.shuaibkarimi.kurdishclothes2.R;
import com.example.shuaibkarimi.kurdishclothes2.services.Services;

import java.util.List;

/**
 * Created by Shuaib Karimi on 10/29/2017.
 */

public class NoticeImageViewPagerAdapter extends PagerAdapter {

    List<NoticeImage> noticeImageList;
    NoticeDetailsActivity context;
    LayoutInflater layoutInflater;
    ImageView imageView;

    public NoticeImageViewPagerAdapter(NoticeDetailsActivity context, List<NoticeImage> noticeImageList) {
        this.noticeImageList = noticeImageList;
        this.context = context;
    }


    @Override
    public int getCount() {
        return this.noticeImageList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.row_details, container, false);

        imageView = (ImageView) view.findViewById(R.id.details_image);


        final NoticeImage noticeImage = noticeImageList.get(position);

        String urlImage = Services.IMAGEURL+noticeImage.getCounter()+'/'+ noticeImage.getImageId();
        Log.e("img url=",urlImage);

        Glide.with(context).load(urlImage)
                .thumbnail(0.5f)
                .crossFade()
                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);

        container.addView(view);

        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
