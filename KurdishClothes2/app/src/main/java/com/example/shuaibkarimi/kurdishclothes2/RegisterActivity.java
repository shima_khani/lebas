package com.example.shuaibkarimi.kurdishclothes2;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.shuaibkarimi.kurdishclothes2.app.AppController;
import com.example.shuaibkarimi.kurdishclothes2.model.User;
import com.example.shuaibkarimi.kurdishclothes2.services.RegisterService;
import com.example.shuaibkarimi.kurdishclothes2.services.Services;
import com.example.shuaibkarimi.kurdishclothes2.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class RegisterActivity extends AppCompatActivity {

    Context context;

    EditText fName, lName, email, pass;
    Button buttonReg;
    RegisterService registerService;
    /*SessionManager sessionManager;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        registerService = new RegisterService(this);
        /*sessionManager = new SessionManager(this);*/

        fName = (EditText) findViewById(R.id.firstName);
        lName = (EditText) findViewById(R.id.lastName);
        email = (EditText) findViewById(R.id.userName);
        pass = (EditText) findViewById(R.id.userPass);
        buttonReg = (Button) findViewById(R.id.btnRegister);

        buttonReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                User user = new User();
                user.setFirstName(fName.getText().toString().trim());
                user.setLastName(lName.getText().toString().trim());
                user.setEmail(email.getText().toString().trim());
                user.setPassword(pass.getText().toString().trim());

                try {
                    registerService.sendToServer(user);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

            }
        });

    }





    /*public void sendToServer(User user) throws JSONException, UnsupportedEncodingException {

        String urlRegister = Services.REGISTER;
        int reqType = Request.Method.POST;

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("firstName", user.getFirstName());
            jsonObject.put("lastName", user.getLastName());
            jsonObject.put("email", user.getEmail());
            jsonObject.put("password", user.getPassword());
        }
        catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(reqType, urlRegister, jsonObject,

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        if (response!=null) {

                            try {

                                parseJson(response);

                            }
                            catch (JSONException e) {

                                e.printStackTrace();
                            }

                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if ((error instanceof NetworkError) || (error instanceof NoConnectionError) ) {

                    Toast.makeText(context,"Network Error", Toast.LENGTH_SHORT).show();
                    // progressDialog.dismiss();
                    return;
                }

                if (error instanceof TimeoutError){

                    Toast.makeText(context, "TimeOut", Toast.LENGTH_SHORT).show();

                    return;
                }

                if ((error instanceof ServerError) || (error instanceof AuthFailureError)){

                    Toast.makeText(context, "ServerError", Toast.LENGTH_SHORT).show();

                    return;
                }

            }
        })
        {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };


        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                12000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        String tag_json_obj = "json_obj_req";

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, tag_json_obj);

    }


    public void parseJson(JSONObject response) throws JSONException {

        boolean res = response.getBoolean("result");

        if (res) {

            *//*long id=response.getLong("id");
            Toast.makeText(context, response.toString(), Toast.LENGTH_LONG).show();
            Toast.makeText(context, "User Register with ID="+id, Toast.LENGTH_LONG).show();*//*
            sessionManager = new SessionManager(context);
            sessionManager.createLoginSession(response.getLong("id"));

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();

        }
        else {

            Toast.makeText(context, "User Not Register ", Toast.LENGTH_LONG).show();
        }

    }*/

}
