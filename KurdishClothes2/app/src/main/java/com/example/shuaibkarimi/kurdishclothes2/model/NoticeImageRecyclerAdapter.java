package com.example.shuaibkarimi.kurdishclothes2.model;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.Target;
import com.example.shuaibkarimi.kurdishclothes2.NoticeDetailsActivity;
import com.example.shuaibkarimi.kurdishclothes2.R;
import com.example.shuaibkarimi.kurdishclothes2.services.Services;

import java.util.List;

/**
 * Created by Shuaib Karimi on 10/26/2017.
 */


public class NoticeImageRecyclerAdapter extends RecyclerView.Adapter<NoticeImageViewHolder> {

    List<NoticeImage> noticeImageList;
    NoticeDetailsActivity mContext;


    public NoticeImageRecyclerAdapter(NoticeDetailsActivity context, List<NoticeImage> feedItemList) {
        this.noticeImageList = feedItemList;
        this.mContext = context;
    }

    @Override
    public NoticeImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_details, parent, false);

        return new NoticeImageViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NoticeImageViewHolder holder, int position) {

        final NoticeImage noticeImage = noticeImageList.get(position);


        String urlImage = Services.IMAGEURL+noticeImage.getCounter()+'/'+ noticeImage.getImageId();
        Log.e("img url=",urlImage);

        Glide.with(mContext).load(urlImage)
                .thumbnail(0.5f)
                .crossFade()
                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return noticeImageList.size();
    }
}