package com.example.shuaibkarimi.kurdishclothes2.services;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.shuaibkarimi.kurdishclothes2.MainActivity;
import com.example.shuaibkarimi.kurdishclothes2.RegisterActivity;
import com.example.shuaibkarimi.kurdishclothes2.app.AppController;
import com.example.shuaibkarimi.kurdishclothes2.model.User;
import com.example.shuaibkarimi.kurdishclothes2.utils.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Created by Shuaib Karimi on 10/23/2017.
 */

public class RegisterService {

    /*private RegisterActivity context;*/

    Context context;

    public RegisterService(Context context) {
        this.context = context;
    }


    public void sendToServer(User user) throws JSONException, UnsupportedEncodingException {

        String urlRegister = Services.REGISTER;
        int reqType = Request.Method.POST;

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("firstName", user.getFirstName());
            jsonObject.put("lastName", user.getLastName());
            jsonObject.put("email", user.getEmail());
            jsonObject.put("password", user.getPassword());
        }
        catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(reqType, urlRegister, jsonObject,

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        if (response!=null) {

                            try {

                                parseJson(response);

                            }
                            catch (JSONException e) {

                                e.printStackTrace();
                            }

                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if ((error instanceof NetworkError) || (error instanceof NoConnectionError) ) {

                    Toast.makeText(context,"Network Error", Toast.LENGTH_SHORT).show();
                    // progressDialog.dismiss();
                    return;
                }

                if (error instanceof TimeoutError){

                    Toast.makeText(context, "TimeOut", Toast.LENGTH_SHORT).show();

                    return;
                }

                if ((error instanceof ServerError) || (error instanceof AuthFailureError)){

                    Toast.makeText(context, "ServerError", Toast.LENGTH_SHORT).show();

                    return;
                }

            }
        })
        {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };


        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                12000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        String tag_json_obj = "json_obj_req";

        AppController.getInstance().addToRequestQueue(jsonObjectRequest, tag_json_obj);

    }


    public void parseJson(JSONObject response) throws JSONException {

        boolean res = response.getBoolean("result");

        if (res) {

            /*long id=response.getLong("id");
            Toast.makeText(context, response.toString(), Toast.LENGTH_LONG).show();
            Toast.makeText(context, "User Register with ID="+id, Toast.LENGTH_LONG).show();*/
            SessionManager session = new SessionManager(context);
            long id=response.getLong("id");
            String userId = String.valueOf(id);
            session.createLoginSession(userId);

            Intent intent = new Intent(context.getApplicationContext(), MainActivity.class);
            // Closing all the Activities
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            context.startActivity(intent);


        }
        else {

            Toast.makeText(context, "User Not Register ", Toast.LENGTH_LONG).show();
        }

    }


}
